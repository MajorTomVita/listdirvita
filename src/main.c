#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_PATH_LENGTH 256

typedef unsigned int u32;
typedef unsigned short u16;

typedef struct {
    u32 id;
    u32 type;
    u32 nfiles;
    u32 unk1;
} bhead;

typedef struct {
	u32 idx;
	char filename[68];
} fname;

typedef struct {
	u32 idx;
	u16 type;
	u16 unk1;
	u32 size;
	u32 unk2;
} finfo;

int main(int argc, char *argv[])
{
    unsigned int totalSize = 0;

    int i,j;
    void *buf = NULL;
    fname *files = NULL;
    finfo *filesInfos = NULL;

    FILE *fp = fopen("files.db", "rb");
    FILE *out = fopen("output.txt", "w+");

    if((fp==NULL) || (out==NULL))
        exit(0);

    fseek(fp, 0, SEEK_END);
    int sz = ftell(fp);
    int nblocks = sz/1024-1;

    buf = malloc(nblocks*1024);
    for(i=0;i<nblocks;i++)
    {
        fseek(fp,i*1024+1024, SEEK_SET); //set cursor to next block
        fread(buf+i*1024, 1024, 1, fp); //read current block
    }
    fclose(fp);

    files = malloc(sizeof(fname)*nblocks*9);
    filesInfos = malloc(sizeof(finfo)*nblocks*9);

    if((files==NULL) || (filesInfos==NULL))
        exit(0);

    for(i=0;i<nblocks;i++)
    {
        bhead bhdr;
        memcpy(&bhdr,buf+1024*i,sizeof(bhead));
        memcpy(&files[i*9],buf+1024*i+0x10,sizeof(fname)*9);
        memcpy(&filesInfos[i*9],buf+1024*i+0x298,sizeof(finfo)*9);

        if(bhdr.type!=0) { //WTF IS THIS STRUCTURE ?!
            memset(&files[i*9], 0, sizeof(fname)*9);
            memset(&filesInfos[i*9], 0xff, sizeof(finfo)*9);
        }
    }
    free(buf);

    for(i=0;i<nblocks*9;i++)
    {
        if(filesInfos[i].idx!=0xffffffff)
        {
            char *path=malloc(sizeof(char)*MAX_PATH_LENGTH);
            memset(path,0,sizeof(char)*MAX_PATH_LENGTH);

            int idx = files[i].idx;
            while(idx!=0) // Not root
            {
                for(j=0;j<nblocks*9;j++)
                {
                    if(filesInfos[j].idx==idx) {
                        char *tmp = malloc(sizeof(char)*MAX_PATH_LENGTH);
                        sprintf(tmp, "%s/%s", files[j].filename, path);
                        free(path);
                        path=tmp;
                        idx=files[j].idx;
                        break;
                    }
                }
            }

            strcat(path, files[i].filename);
            if(filesInfos[i].type==0x8000) //directory
                strcat(path,"/");

            if(strlen(path) > 0) {
                printf("%s (%d bytes)\n", path, filesInfos[i].size);
                strcat(path,"\n");
                fwrite(path, strlen(path), 1, out);
            }

            free(path);

            totalSize += filesInfos[i].size;
        }
    }
    printf("\n----------------------------------------\nTotal size : %d kb (%d mb)\n", totalSize / 1024, totalSize / (1024*1024));
    fclose(out);
    return 0;
}

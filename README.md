# ListDirVita #

ListDirVita is a simple parser for files.db (PSVita filesystem).

## How do I get set up? ##

* Put files.db next to listdirvita.exe
* Start listdirvita.exe from terminal/command prompt
* Grab directory listing

## Contribution guidelines ##

* Find files.db structure (not fully known)
* Write ListDirVita (still WIP)
* Profit.

## Current files.db structure ##

files.db is organized in blocks of 1024 bytes, the first block is the header of the file, its structure is still unknown and unnecessary for this tool.

Each block contains 4 sub-blocks :

* Block header
* Filenames (9 entries)
* File informations (9 entries)
* Hashes block?

### Block header ###

0x10 bytes wide, structure is uncertain.


```
#!c

 typedef struct {
	 u32 id;
	 u32 type;
	 u32 nfiles;
	 u32 unk1;
 } bhead;
```


**id** :

* 0xYY : Block identification ? generally 0x02
* 0xffffffff : ???


**type**

* 0x00 : The block will describe files and parent directories.
* 0x01 : ??? Ignore, changes structure of the current block.


**nfiles** :

One block describes up to 9 files, although it can be less, nfiles is the number of files described in the current block.

**unk1** :

Padding ?


### Filenames (9 entries) ###

9 entries of 72 bytes, describing filename and parent directory.


```
#!c

 typedef struct {
	 u32 idx;
	 char filename[68];
 } fname;
```


**idx** :

Describes parent directory of current file entry. (Cf. File informations block)


**filename** :

Filename of current file entry (68 bytes, null terminated).


### File informations (9 entries) ###

9 entries of 0x10 bytes.


```
#!c

 typedef struct {
	 u32 idx;
	 u16 type;
	 u16 unk1;
	 u32 size;
	 u32 unk2;
 } finfo;

```

**idx** :


Used to retrieve parent directory for filenames block, each file entry has its own index, 0 is for root.

*Example* : 

| File/Dir  | idx (fname) | idx (finfo)| Path                 |
| ----------|-------------|------------|----------------------|
| lol.png   | 2           | 3          | /images/abcd/lol.png |
| abcd/     | 1           | 2          | /images/abcd/        |
| images/   | 0           | 1          | /images/             |
| eboot.bin | 0           | 4          | /eboot.bin           |


**type** :

* 0x01   = normal file
* 0x8000 = directory
* 0x4006 = unencrypted system file 
* 0x06   = encrypted system file


**unk1** :

???


**size** :

Size of the current file.


**unk2** :


Padding ?



## Credits ##

* Major_Tom